// Simple echo bot

package main

import (
    "flag"
    "log"
    "strconv"
    "time"

    tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var (
    NOTIFY_CHAT_ID  = ""
    BOT_TOKEN       = ""
)

var (
    notify  = flag.String("notify",  "", "Send notification with message attached.")
    command = flag.Bool("command", false, "Start an echo server and listen for incoming messages and commands")
    debug   = flag.Bool("debug",   false, "Debug mode")
)

func main() {
    flag.Parse()
    var N_CHAT_ID_S int64
    if nn, err := strconv.ParseInt(NOTIFY_CHAT_ID, 10, 64); err == nil {
        N_CHAT_ID_S = nn
    } else {
        log.Fatalf("Error -:- strconv.ParseInt : %v\n", err)
    }

    bot, err := tgbotapi.NewBotAPI(BOT_TOKEN)
    if err != nil {
        log.Fatalf("Error -:- tgbotapi.NewBotAPI : %v\n", err)
    }
    bot.Debug = *debug
    log.Printf("Authorized on account %s", bot.Self.UserName)

    if *notify != "" {
        log.Printf("Sending notification message : '%v'\n", *notify)
        msg := tgbotapi.NewMessage(N_CHAT_ID_S, *notify)
        bot.Send(msg)

    } else if *command {
        log.Printf("Entering echo mode\n")
        u := tgbotapi.NewUpdate(0)
        u.Timeout = 60

        updates, err := bot.GetUpdatesChan(u)
        if err != nil {
            log.Fatalf("Error -:- bot.GetUpdatesChan : %v\n", err)
        }

        // Optional: wait for updates and clear them if you don't want to handle
        // a large backlog of old messages
        time.Sleep(time.Millisecond * 500)
        updates.Clear()

        for update := range updates {
            if update.Message == nil {
                continue
            }

            log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

            msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
            msg.ReplyToMessageID = update.Message.MessageID

            bot.Send(msg)
        }
    }
}
