// Simple echo bot

package main

import (
    "flag"
    "fmt"
    "log"
    "os"
)

var (
    notify = flag.String("notify",  "", "Send notification with message attached.")
    debug  = flag.Bool("debug", false, "Print debug information")
    config = flag.String("config", DefaultConfigPath, "Path to configuration file to use")

    synopsis = "Utilitaire pour envoyer des messages nextcloud talk en utilisant un Bot."
    uString  = "go-bot -notify <MESSAGE> [-config </PATH/TO/CONFIG/FILE>] [-debug]"

    details = `Cet utilitaire simple permet d'envoyer des messages par nextcloud Talk en utilisant
un bot conversationnel. Le bot doit bien évidemment déjà exister et l'utilisateur
de cet outil doit connaître le token du bot à utiliser. De plus, ce bot doit être
dans une conversation afin de pouvoir y envoyer des messages. 

La configuration de l'outils doit obligatoirement contenir les informations suivantes :
  1. bottoken  : Le token du bot à utiliser. Ce token correspond à celui qui a
                permis la création du bot.
  2. chatid    : L'identifiant de la salle de conversation Nextcloud dans 
                laquelle envoyer les messages.
  3. ncbase    : L'hyperlien pointant sur l'instance de nextcloud à utiliser.

De plus, les informations suivantes peuvent être configurées en option et seront
complétées avec des valeurs par défaut si omises : 
  1. ncapipath : Lien de base de l'API à utiliser pour faire l'envoie de messages.

Par défaut, le programme s'attend à trouver la configuration dans le HOME de 
l'utilisateur. Il est cependant possible d'en spécifier une selon le cas. 
Celle-ci doit être au format JSON. Chaque paramètre est une paire "clé":"valeur".

Les différentes clés correspondent à celles mentionnées plus haut. 
`
    Usage = func() {
        fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", synopsis)
        fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uString)
    
        fmt.Fprintf(flag.CommandLine.Output(), "Arguments de %s :\n", os.Args[0])
        flag.PrintDefaults()
    
        fmt.Printf("\nDescription détaillée :\n%v\n", details)
    }
)

func main() {
    flag.Usage = Usage
    flag.Parse()
    if *notify == "" {
        log.Println("Erreur : Message à envoyer manquant.")
        flag.Usage()
        os.Exit(1)
    }
    conf := NewConfig()
    err := conf.LoadJSONConfig(*config)
    if err != nil {
        log.Fatalf("Error : Could not properly load configuration file : %v\n", err)
    }
    
    status := conf.ConfigOK()
    if status != OK {
        log.Fatalf("Error (%v) : Configuration file is incomplete. Please check and make sure all required fields are present and filled.", status)
    }

    if *debug {
        log.Println("Configuration reçue : ")
        conf.Show()
    }

    ncUrl := BuildURL((*conf).NCBase, (*conf).NCAPIPath, (*conf).ChatID, "message")
    random := GenerateRandomBytes(64)
    signature := NewSHA256HMACString(*notify, (*conf).BotToken, random)

    if *debug {
        log.Printf(`Paramètres du message à envoyer :
  > URL           : %s
  > Message       : %s
  > Clé aléatoire : %s
  > Signature     : %s
`, ncUrl, *notify, random, signature)
    }
    err = Notify(ncUrl, *notify, string(random), string(signature))
    if err != nil {
        log.Fatalf("Error : Failed to send message : %v\n", err)
    }
}
