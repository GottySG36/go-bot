// Utilities to prepare and send messages to a Nextcloud instance

package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"math/rand"
	"net/http"
	"strings"
)

var randChoices = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890")

func GenerateRandomBytes(n int) []byte {
	key := make([]byte, n, n)
	for i := 0; i < n; i++ {
		key[i] = randChoices[rand.Intn(len(randChoices)-1)]
	}
	return key
}

func NewSHA256HMACString(msg, key string, random []byte) string {
	mac := hmac.New(sha256.New, []byte(key))
	concat := make([]byte, len(msg)+len(random), len(msg)+len(random))
	copy(concat[0:len(random)], random)
	copy(concat[len(random):], msg)
	mac.Write(concat)
	return hex.EncodeToString(mac.Sum(nil))
}

func BuildURL(base, api, id, rem string) string {
	var build strings.Builder
	build.WriteString(base)
	if base[len(base)-1] != '/' {
		build.WriteByte('/')
	}

	if api[0] == '/' {
		build.WriteString(api[1:])
	} else {
		build.WriteString(api)
	}
	if api[len(api)-1] != '/' {
		build.WriteByte('/')
	}

	if id[0] == '/' {
		build.WriteString(id[1:])
	} else {
		build.WriteString(id)
	}
	if id[len(id)-1] != '/' {
		build.WriteByte('/')
	}

	if rem[0] != '/' {
		build.WriteByte('/')
	}
	build.WriteString(rem)

	return build.String()
}

func Notify(uri, msg, random, signature string) error {
	data := map[string]string{
		"message": msg,
	}
	body, _ := json.Marshal(data)
	r, err := http.NewRequest("POST", uri, bytes.NewReader(body))
	if err != nil {
		return err
	}

	r.Header.Set("Content-Type", "application/json")
	r.Header.Set("OCS-APIRequest", "true")
	r.Header.Set("X-Nextcloud-Talk-Bot-Random", random)
	r.Header.Set("X-Nextcloud-Talk-Bot-Signature", signature)

	client := http.Client{}
	client.Do(r)
	_, err = client.Do(r)
	if err != nil {
		return err
	}
	return nil
}
