// Utility to read and import a JSON configuration file

package main

import (
    "encoding/json"
    "fmt"
    "os"
)

type Status int

var (
    DefaultConfigName = ".go.bot.json"
    DefaultConfigPath = fmt.Sprintf("~/%s", DefaultConfigName)
)

const (
    OK Status = iota
    Incomplete
)

type Config struct {
    BotToken  string `json:"bottoken"`
    ChatID    string `json:"chatid"`
    NCBase    string `json:"ncbase"`
    NCAPIPath string `json:"ncapipath"`
}

var DefaultConfig = Config{
    NCAPIPath: "/ocs/v2.php/apps/spreed/api/v1/bot",
}

func NewConfig() *Config {
    conf := DefaultConfig
    return &conf
}

func (c *Config) LoadJSONConfig(path string) error {
    if path == DefaultConfigPath {
        home, err := os.UserHomeDir()
        if err != nil {
            return err
        }
        path = fmt.Sprintf("%s/%s", home, DefaultConfigName)
    }
    jsonData, err := os.ReadFile(path)
    if err != nil {
        return err
    }
    
    err = json.Unmarshal(jsonData, c)
    return err
}

func (c *Config) ConfigOK() Status {
    if (*c).BotToken == "" || (*c).ChatID == "" || (*c).NCBase == "" {
        return Incomplete
    }
    return OK
}

func (c *Config) Show() {
    fmt.Printf(`  > BotToken : %v
  > ChatID   : %v
  > NCBase   : %v
`, (*c).BotToken, (*c).ChatID, (*c).NCBase)
}

